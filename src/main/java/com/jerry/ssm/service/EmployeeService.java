package com.jerry.ssm.service;

import com.github.pagehelper.PageInfo;
import com.jerry.ssm.pojo.Employee;

import java.util.List;

/**
 * ClassName: EmployeeService
 * Package: com.jerry.ssm.service
 * Description:
 *
 * @Author jerry_jy
 * @Create 2023-02-11 9:35
 * @Version 1.0
 */
public interface EmployeeService  {
    /**
     * 查询所有员工信息
     * @return
     */
    List<Employee> getAllEmployee();

    /**
     * 获取员工的分页信息
     * @param pageNum
     * @return
     */
    PageInfo<Employee> getEmployeePage(Integer pageNum);


    /**
     * 根据id查询员工信息
     * @return
     */
    Employee selectEmployeeInfoById(Integer id);
}
