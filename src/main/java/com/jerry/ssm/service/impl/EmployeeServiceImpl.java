package com.jerry.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jerry.ssm.mapper.EmployeeMapper;
import com.jerry.ssm.pojo.Employee;
import com.jerry.ssm.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ClassName: EmployeeServiceImpl
 * Package: com.jerry.ssm.service.impl
 * Description:
 *
 * @Author jerry_jy
 * @Create 2023-02-11 9:36
 * @Version 1.0
 */

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public List<Employee> getAllEmployee() {
        return employeeMapper.getAllEmployee();
    }

    @Override
    public PageInfo<Employee> getEmployeePage(Integer pageNum) {
        //开启分页功能
        PageHelper.startPage(pageNum, 5);
        //查询所有的员工信息
        List<Employee> list = employeeMapper.getAllEmployee();
        //获取分页相关数据
        PageInfo<Employee> pageInfo = new PageInfo<>(list, 5);
        return pageInfo;
    }

    @Override
    public Employee selectEmployeeInfoById(Integer id) {
        return employeeMapper.selectEmployeeInfoById(id);
    }

}
